What would need to happen next is the following:
- make little option-circles appear a little ways out from the main-circle
- animate the little option-circles going out   
- adjust direction that circles go out based on if the circles would go
  off screen.

Started From:

Combination of the following sites:

- http://www.raywenderlich.com/63089/cookbook-moving-table-view-cells-with-a-long-press-gesture

- http://stackoverflow.com/questions/6589265/how-to-draw-a-custom-uiview-that-is-just-a-circle-iphone-app

