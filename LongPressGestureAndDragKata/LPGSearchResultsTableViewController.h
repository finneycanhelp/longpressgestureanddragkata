//
//  LPGSearchResultsTableViewController.h
//  LongPressGestureAndDragKata
//
//  Created by Michael Finney on 7/9/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPGSearchResultsTableViewController : UITableViewController

@end
