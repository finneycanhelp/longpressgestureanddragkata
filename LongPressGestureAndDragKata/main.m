//
//  main.m
//  LongPressGestureAndDragKata
//
//  Created by Michael Finney on 7/9/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LPGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LPGAppDelegate class]));
    }
}
