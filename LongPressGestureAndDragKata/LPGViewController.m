//
//  LPGViewController.m
//  LongPressGestureAndDragKata
//
//  Created by Michael Finney on 7/9/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import "LPGViewController.h"

@interface LPGViewController ()

@end

@implementation LPGViewController

- (void)viewDidLoad
{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [self.view addGestureRecognizer:longPress];

    
}

- (IBAction)longPressGestureRecognized:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
//    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.view];
    
    NSLog(@"location x: %f y: %f", location.x, location.y);
    
    
}


@end
