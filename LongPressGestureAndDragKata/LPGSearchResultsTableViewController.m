//
//  LPGSearchResultsTableViewController.m
//  LongPressGestureAndDragKata
//
//  Created by Michael Finney on 7/9/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import "LPGSearchResultsTableViewController.h"

@interface LPGSearchResultsTableViewController ()

@property UIView *circle;

@end

@implementation LPGSearchResultsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [self.tableView addGestureRecognizer:longPress];
    
    self.circle = [self createCircleAtPoint:CGPointMake(100, 100)];
    self.circle.hidden = YES;
    [self.view addSubview:self.circle];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchResult" forIndexPath:indexPath];
    
    cell.textLabel.text = @(indexPath.row).stringValue;
    
    return cell;
}


- (IBAction)longPressGestureRecognized:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {

                NSLog(@"%d", (int)indexPath.row);

                [self moveCircleToPoint:location];
                self.circle.hidden = NO;
                
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            
            break;
        }
            
        default: {
            
            break;
        }
    }
}

- (void)moveCircleToPoint:(CGPoint)point;
{
    CGFloat frameHeightOrWidth = 50; // FIXME: make constant
    CGFloat cornerRadius = frameHeightOrWidth / 2; // FIXME: make constant

    CGFloat x = point.x - cornerRadius;
    CGFloat y = point.y - cornerRadius;
    
    CGRect myFrame = self.circle.frame;
    myFrame.origin.x = x;
    myFrame.origin.y = y;
    self.circle.frame = myFrame;
}

- (UIView *)createCircleAtPoint:(CGPoint)point
{
    
    // x has to be moved left by radius (subtract) from given point
    // y has to be moved up by radius (subtract) from given point

    // issue with negative numbers?
    
    CGFloat frameHeightOrWidth = 50;
    CGFloat cornerRadius = frameHeightOrWidth / 2;
    CGFloat x = point.x - cornerRadius;
    CGFloat y = point.y - cornerRadius;
    
    UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake(x, y, frameHeightOrWidth, frameHeightOrWidth)];
    circleView.alpha = 0.5;
    circleView.layer.cornerRadius = cornerRadius;
    circleView.backgroundColor = [UIColor blueColor];
    return circleView;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
